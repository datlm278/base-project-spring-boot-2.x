package com.datlm.baseproject.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    @NotEmpty
    private String fullName;

    @NotEmpty
    private String username;

    @Email
    private String email;

    @NotEmpty
    private String userAD;

}
