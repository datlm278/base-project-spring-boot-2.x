package com.datlm.baseproject.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/base")
public class BaseController {

    @GetMapping(value = "/hello-world")
    public ResponseEntity<String> helloWorld() {
        return ResponseEntity.ok().body("\"Hello world\"");
    }

}
