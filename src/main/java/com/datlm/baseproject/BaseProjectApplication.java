package com.datlm.baseproject;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableEncryptableProperties
public class BaseProjectApplication {

	private static final Logger L = LoggerFactory.getLogger(BaseProjectApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(BaseProjectApplication.class, args);

		L.info("-----------------------------------------------------------");
		L.info("            Welcome to New APP Service.");
		L.info("           Application start successfully.");
		L.info("-----------------------------------------------------------");

	}

}
