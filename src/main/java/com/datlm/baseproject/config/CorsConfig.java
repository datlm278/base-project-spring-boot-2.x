package com.datlm.baseproject.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


@Configuration
public class CorsConfig {
    private static final Logger L = LoggerFactory.getLogger(CorsConfig.class);

    @Value("${method.allowed}")
    private String allowedMethod;

    @Value("${cors.origins}")
    private String corsOrigins;

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {

        L.info("cors origin: {}", corsOrigins);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        CorsConfiguration corsConfig = new CorsConfiguration();
        corsConfig.setAllowCredentials(true);
        corsConfig.addAllowedOrigin(corsOrigins);
        corsConfig.addAllowedHeader("*");
        corsConfig.addAllowedMethod(allowedMethod);
        corsConfig.addExposedHeader(HttpHeaders.AUTHORIZATION);

        source.registerCorsConfiguration("/**", corsConfig);

        return source;
    }
}
