package com.datlm.baseproject.config;

import lombok.Generated;
import org.hibernate.mapping.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

//@Configuration
//@EnableSwagger2
//@Generated
public class SwaggerConfig {

    private final Logger L = LoggerFactory.getLogger(SwaggerConfig.class);

    @Bean
    public Docket api() {

        L.info("aaaaaaaaaaaaaaaaaaaa");

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
//                .securityContexts(Collections.singletonList(securityContext()))
//                .securitySchemes(Collections.singletonList(apiKey()))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.datlm.baseproject.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiKey apiKey() {
        return null;
    }

    private SecurityContext securityContext() {
        return null;
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Base Project - backend API",
                "Description of Base Project backend API.",
                "1.0",
                "Terms of service",
                new Contact("datlm", "", "datlm278@outlook.com"),
                "License of MBBank",
                "API license URL",
                Collections.emptyList()
        );
    }

}
