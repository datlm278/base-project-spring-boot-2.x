package com.datlm.baseproject.repository;

import com.datlm.baseproject.entity.base.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseRepository extends JpaRepository<BaseEntity, Long> {
}
