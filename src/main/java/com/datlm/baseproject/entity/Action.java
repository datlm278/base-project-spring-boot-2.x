package com.datlm.baseproject.entity;

import com.datlm.baseproject.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "action")
public class Action extends BaseEntity {

    @Column(name = "action_name")
    private String actionName;

    private String path;

    private String controller;

    private String method;

    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "action")
    private List<RoleAction> roleActions;
}
